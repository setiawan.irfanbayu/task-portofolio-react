import About from "./components/About";
import Contact from "./components/Contact";
import Hero from "./components/Hero";
import Navbar from "./components/Navbar";
import Portofolio from "./components/Portofolio";

function App() {
  return (
    <div>
      <Navbar />
      <Hero />
      <Portofolio />
      <About />
      <Contact />
    </div>
  );
}

export default App;
